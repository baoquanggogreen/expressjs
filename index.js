const express = require('express');
const app = express();
const port = 8080;

app.get('/user', function(request, response) {
    response.send("<h1>Hi</h1>");
})
app.listen(port, function() {
    console.log('Running on port ' + port);
});